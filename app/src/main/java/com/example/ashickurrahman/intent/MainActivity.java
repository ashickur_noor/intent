package com.example.ashickurrahman.intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button second;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        second = findViewById(R.id.startSecond);
        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, secondActivity.class);
                startActivity(intent);
            }

            //Onclick লিখলেই পরের অংশ চলে আসবে।
        });


    }

//
//    public void startSecond(View view) {
//        Intent intent = new Intent(MainActivity.this, secondActivity.class);
//        startActivity(intent);
//    }
}
